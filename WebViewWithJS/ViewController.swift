//
//  ViewController.swift
//  WebViewWithJS
//
//  Created by Eunchan Kim on 2023/05/24.
//


import UIKit
import WebKit

class ViewController: UIViewController {

    @IBOutlet weak var webViewBackgroundView: UIView!
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setWebView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        load()
    }

    func setWebView() {
        let contentController = WKUserContentController()
        
        // Bridge 등록
        contentController.add(self, name: "back")
        contentController.add(self, name: "outLink")
        contentController.add(self, name: "toast")
        
        let configuration = WKWebViewConfiguration()
        configuration.userContentController = contentController
        
        webView = WKWebView(frame: .zero, configuration: configuration)
        webViewBackgroundView.addSubview(webView)
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.topAnchor.constraint(equalTo: webViewBackgroundView.topAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: webViewBackgroundView.bottomAnchor).isActive = true
        webView.leadingAnchor.constraint(equalTo: webViewBackgroundView.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: webViewBackgroundView.trailingAnchor).isActive = true
    }

    func load() {
        if let url = Bundle.main.url(forResource: "Example", withExtension: "html") {
            webView.loadFileURL(url, allowingReadAccessTo: url)
        }
    }
}

extension ViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
        case "back":
            let alert = UIAlertController(title: nil, message: "Back 버튼 클릭", preferredStyle: .alert)
            let action = UIAlertAction(title: "확인", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        case "toast":
            print("토스트누름!")
            let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
            toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            toastLabel.textColor = UIColor.white
//            toastLabel.font = font
            toastLabel.textAlignment = .center;
            toastLabel.text = "message"
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 10;
            toastLabel.clipsToBounds = true
            self.view.addSubview(toastLabel)
            UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut,
               animations: { toastLabel.alpha = 0.0 },
               completion: {(isCompleted) in toastLabel.removeFromSuperview() }
            )

        case "outLink":
            guard let outLink = message.body as? String, let url = URL(string: outLink) else {
                return
            }
            
            let alert = UIAlertController(title: "OutLink 버튼 클릭", message: "URL : \(outLink)", preferredStyle: .alert)
            let openAction = UIAlertAction(title: "링크 열기", style: .default) { _ in
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            let cancelAction = UIAlertAction(title: "취소", style: .cancel, handler: nil)
            alert.addAction(openAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        default:
            break
        }
    }
}
